variable "project_id" {
  type        = string
  default     = "automl-cloud-409621"
  description = "The project ID for Google Cloud"
}

variable "region" {
  type        = string
  default     = "europe-west1"
  description = "The region for resources"
}

variable "zone" {
  type        = string
  default     = "europe-west1-b"
  description = "The zone for resources"
}
terraform {
    required_version = ">= 0.12"
    required_providers {
        google = {
            source = "hashicorp/google"
            version = "3.5.0"
        }
    }
}

provider "google" {
  project = var.project_id
  region  = var.region
  # athentication file
  credentials = file("automl-cloud-409621-7bab86fdaecd.json")
}

# create a bucket named "automl-cloud-409621"
resource "google_storage_bucket" "bucket" {
  name     = "automl-cloud-409621"
  location = "EU"
  force_destroy = true

  lifecycle_rule {
    condition {
      age = 3
    }
    action {
      type = "Delete"
    }
  }
}